# Swift Benchmarker

A way to benchmark your algorithms in Swift easily and quickly!

If you want to use it in an existing project of yours, copy *Benchmarker.swift* to your project and use the class as demoed in *ViewController.swift*.


To truly test the benchmark times, you will probably want to set your *Swift Compiler* settings to `Whole Module` for *Compilation Mode* and `Optimize for Speed` for *Optimization Level*. 
