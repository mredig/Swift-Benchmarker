//
//  Benchmarker.swift
//  Benchmark
//
//  Created by Michael Redig on 6/17/18.
//  Copyright © 2018 Michael Redig. All rights reserved.
//

import Foundation


class Benchmarker {
	
	
	static func benchmarkThis(named name: String?, withIterations iterations: UInt64, andClosure closure: (_ iteration: UInt64)->()) {
		let startTime = CFAbsoluteTimeGetCurrent()
		for i in 0..<iterations {
			closure(i)
		}
		let endTime = CFAbsoluteTimeGetCurrent()
		let elapsed = endTime - startTime
		
		if let benchLabel = name {
			print("Benchmark \(benchLabel) took \(elapsed) seconds")
		} else {
			print("This benchmark took \(elapsed) seconds.")
		}
	}
	
}
