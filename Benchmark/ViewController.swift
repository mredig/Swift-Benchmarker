//
//  ViewController.swift
//  Benchmark
//
//  Created by Michael Redig on 6/17/18.
//  Copyright © 2018 Michael Redig. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	
	func runBenchmarks() {
		
		let iterations: UInt64 = 1000000

		Benchmarker.benchmarkThis(named: "sqrt", withIterations: iterations) { (i) in

			let pointA = CGPoint(x: 101.35, y: 80.54)
			let pointB = CGPoint(x: 342.76, y: 389.9625)
			
			let distance = sqrt((pointB.x - pointA.x) * (pointB.x - pointA.x) + (pointB.y - pointA.y) * (pointB.y - pointA.y))
			
			if distance < 5 {
				print("less than 5")
			}
		}
		
		Benchmarker.benchmarkThis(named: "pow", withIterations: iterations) { (i) in
			let pointA = CGPoint(x: 101.35, y: 80.54)
			let pointB = CGPoint(x: 342.76, y: 389.9625)
			
			
			let distance = sqrt(pow(pointB.x - pointA.x, 2) + pow(pointB.y - pointA.y, 2))
			
			if distance < 5 {
				print("less than 5")
			}
		}
	
		Benchmarker.benchmarkThis(named: "hypot", withIterations: iterations) { (i) in
			let pointA = CGPoint(x: 101.35, y: 80.54)
			let pointB = CGPoint(x: 342.76, y: 389.9625)
			
			
			let distance = hypot(pointB.x - pointA.x, pointB.y - pointA.y)
			
			if distance < 5 {
				print("less than 5")
			}
		}
		
		Benchmarker.benchmarkThis(named: "hypotf", withIterations: iterations) { (i) in
			let pointA = CGPoint(x: 101.35, y: 80.54)
			let pointB = CGPoint(x: 342.76, y: 389.9625)
			
			
			let distance = hypotf(Float(pointB.x) - Float(pointA.x), Float(pointB.y) - Float(pointA.y))
			
			if distance < 5 {
				print("less than 5")
			}
		}
		
		let pointA = CGPoint(x: 101.35, y: 80.54)
		let pointB = CGPoint(x: 342.76, y: 389.9625)
		
		
		let distanceSqrt = sqrt((pointB.x - pointA.x) * (pointB.x - pointA.x) + (pointB.y - pointA.y) * (pointB.y - pointA.y))
		let distancePow = sqrt(pow(pointB.x-pointA.x,2)+pow(pointB.y-pointA.y,2))
		let distanceHypot = hypot(pointB.x - pointA.x, pointB.y - pointA.y)
		let distanceHypotf = hypotf(Float(pointB.x) - Float(pointA.x), Float(pointB.y) - Float(pointA.y))

		
		print(distanceSqrt, distancePow, distanceHypot, distanceHypotf)
		
	}
	
	

	@IBAction func buttonPressed(_ sender: UIButton) {
		runBenchmarks()
	}
	
}

